terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "us-west-2"
}


resource "aws_iam_role" "prod-ci-role" {
  name = "prod-ci-role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}


resource "aws_iam_policy" "prod-ci-policy" {
  name        = "prod-ci-policy"
  description = "Production CI Policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "my-policy-attach" {
  role = "${aws_iam_role.prod-ci-role.name}"
  policy_arn = "${aws_iam_policy.prod-ci-policy.arn}"
}

resource "aws_iam_group" "prod-ci-group" {
  name = "prod-ci-group"
}

resource "aws_iam_user" "prod-ci-user" {
  name = "prod-ci-user"
}

resource "aws_iam_group_policy_attachment" "prod-ci-attach" {
  group      = aws_iam_group.prod-ci-group.name
  policy_arn = aws_iam_policy.prod-ci-policy.arn
}

resource "aws_iam_user_group_membership" "prod-ci-group-membership" {
  user = aws_iam_user.prod-ci-user.name

  groups = [
    aws_iam_group.prod-ci-group.name,
  ]
}


