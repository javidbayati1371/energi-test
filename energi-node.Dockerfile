ARG PACKAGE_NAME=energi3-v3.1.3-linux-armv8
ARG PUBLIC_KEY=C43E788A1BD03C280CAC7D9FF5047BCFDB1DA83F
ARG VALID_KEY="F5047BCFDB1DA83F Ryan Lucchese <ryan@energi.team>"
ARG UBUNTU_MAGE_URL=ubuntu
ARG UBUNTU_IMAGE_VERSION=22.10

FROM ubuntu:22.10

ARG PACKAGE_NAME
ARG PUBLIC_KEY
ARG VALID_KEY

WORKDIR /energi

RUN apt-get update && apt-get -y install gpg

ADD https://s3-us-west-2.amazonaws.com/download.energi.software/releases/energi3/v3.1.3/SHA256SUMS .
ADD https://s3-us-west-2.amazonaws.com/download.energi.software/releases/energi3/v3.1.3/SHA256SUMS.asc .
ADD https://s3-us-west-2.amazonaws.com/download.energi.software/DB1DA83F.asc .
ADD https://s3-us-west-2.amazonaws.com/download.energi.software/releases/energi3/v3.1.3/${PACKAGE_NAME}.tgz .

RUN gpg --show-keys DB1DA83F.asc 2>/dev/null | grep -q ${PUBLIC_KEY} || exit 1
RUN gpg --import DB1DA83F.asc

# Based on (https://rakhesh.com/docker/verifying-downloaded-files-using-their-pgp-asc-signatures-in-docker)
RUN gpg --status-fd 1 --verify SHA256SUMS.asc SHA256SUMS 2>/dev/null | grep -q "GOODSIG ${VALID_KEY}" || exit 1

RUN sha256sum --check --ignore-missing SHA256SUMS
RUN tar zxvf ${PACKAGE_NAME}.tgz && mv ./${PACKAGE_NAME}/* . && rm -rf ${PACKAGE_NAME}*

ENTRYPOINT [ "/bin/sh","-ecx","bin/energi3" ]